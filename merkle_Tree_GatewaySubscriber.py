#!/usr/bin/python

# Copyright (c) 2010-2013 Roger Light <roger@atchoo.org>
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Distribution License v1.0
# which accompanies this distribution. 
#
# The Eclipse Distribution License is available at 
#   http://www.eclipse.org/org/documents/edl-v10.php.
#
# Contributors:
#    Roger Light - initial implementation
# Copyright (c) 2010,2011 Roger Light <roger@atchoo.org>
# All rights reserved.

# This shows a simple example of an MQTT subscriber.

import sys, json, hashmap
import MySQLdb
try:
    import paho.mqtt.client as mqtt
except ImportError:
    # This part is only required to run the example from within the examples
    # directory when the module itself is not installed.
    #
    # If you have the module installed, just use "import paho.mqtt.client"
    import os
    import inspect
    cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../src")))
    if cmd_subfolder not in sys.path:
        sys.path.insert(0, cmd_subfolder)
    import paho.mqtt.client as mqtt


username = "Merkle_Gateway"
PASSWORD = "12345"

iotSecurity/Merkle - publish on 
iotSecurity/# - subscribe

table name: merkle_hash
Columns   : device_id device_name, old_hash, new_hash

the device is sending me this json : msg_payload = json.dumps({'humidity':10, 'temperature':20, 'sha256': 'hash2'})

# you must create a Cursor object. It will let
#  you execute all the queries you need

def on_connect(mqttc, obj, flags, rc):
    print("rc: "+str(rc))
  
def on_message(mqttc, obj, msg):

    db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user="root",         # your username
                     passwd="whitehats",  # your password
                     db="iotsec")        # name of the data base
    cur = db.cursor()
    cur.execute("SELECT device_name from merkle_hash")
    for row in cur.fetchall():
        unique_device_hash[row[0]] = 0

    device_count = len(unique_device_hash)

    while device_count!=0:

        if unique_device_hash[(msg.topic).split('/')[1]] == 0:
            unique_device_hash[(msg.topic).split('/')[1]] = 1
            
            data = json.loads(msg.payload)
            

            if('sha256' in data):
                new_hash = str(data["sha256"]) 
                device_id = (msg.topic).split('/')[1]
                cur.execute("UPDATE merkle_hash set device_name = '" +device_id+ "' , old_hash = '" +new_hash+ "'  ")        
                device_count = device_count - 1

            if device_count == 0
               cur.execute("SELECT new_hash from merkle_hash where device_id = '" +device_id + "' ")
                for row in cur.fetchall():
                        returned_hash = row[0]
			   if new_hash != returned_hash:
				pub_payload = json.dumps({'device_id':device_id})
                        	publishTopic = "iotSecurity/merkle"
				mqttc.publish(publishTopic, payload=pub_payload, qos=0, retain=False)
    db.close()


    if msg.topic == "iotSecurity/merkle":
	data = json.loads(msg.payload)
        if('sha256' in data):
	 	my_hash = str(data["sha256"])

	db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user="root",         # your username
                     passwd="whitehats",  # your password
                     db="iotsec")        # name of the data base
   	 cur = db.cursor()
    	 cur.execute("SELECT new_hash from merkle_hash where device_id in (select device_name from merkle_hash")
    	   for row in cur.fetchall():
		saved_hash = row[0]	
	   if saved_hash != my_hash
		print "Device hached"
		publishTopic = "iotSecurity/merkle"
               	pub_payload = json.dumps({'device_id':device_id, 'setStatus':'hacked'})
                mqttc.publish(publishTopic, payload=pub_payload, qos=0, retain=False)		


def on_publish(mqttc, obj, mid):
    print("mid: "+str(mid))

def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))
	
def on_log(mqttc, obj, level, string):
    print(string)


# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
mqttc = mqtt.Client()
mqttc.username_pw_set(username, password=PASSWORD)
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe

# Uncomment to enable debug messages
#mqttc.on_log = on_log

mqttc.connect("192.168.0.21", 1883, 60)
mqttc.subscribe("iotSecurity/#", 0)

mqttc.loop_forever()

