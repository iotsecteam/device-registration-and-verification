#!/usr/bin/python

# Copyright (c) 2010-2013 Roger Light <roger@atchoo.org>
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Distribution License v1.0
# which accompanies this distribution. 
#
# The Eclipse Distribution License is available at 
#   http://www.eclipse.org/org/documents/edl-v10.php.
#
# Contributors:
#    Roger Light - initial implementation
# Copyright (c) 2010,2011 Roger Light <roger@atchoo.org>
# All rights reserved.

# This shows a simple example of an MQTT subscriber.

import sys, json
import MySQLdb
try:
    import paho.mqtt.client as mqtt
except ImportError:
    # This part is only required to run the example from within the examples
    # directory when the module itself is not installed.
    #
    # If you have the module installed, just use "import paho.mqtt.client"
    import os
    import inspect
    cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../src")))
    if cmd_subfolder not in sys.path:
        sys.path.insert(0, cmd_subfolder)
    import paho.mqtt.client as mqtt


username = "verification"
PASSWORD = "12345"


# you must create a Cursor object. It will let
#  you execute all the queries you need

def on_connect(mqttc, obj, flags, rc):
    print("rc: "+str(rc))
  
def on_message(mqttc, obj, msg):
    if msg.topic != "iotSecurity/deviceHacked" and msg.topic != "iotSecurity/deviceState":
    	print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))
    	print(str(msg.payload))
    	device_id = (msg.topic).split('/')[1]
	print ("device id is : " +device_id)
	data = json.loads(msg.payload)
    	if('sha256' in data):
    		print("printing sha hash: " +data["sha256"])
        	my_hash = str(data["sha256"])
		db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user="root",         # your username
                     passwd="whitehats",  # your password
                     db="iotsec")        # name of the data base
		cur = db.cursor()
		print my_hash
        	cur.execute("SELECT hash_value from device_hash where device_name = '"+device_id+"'")
        	if(cur.rowcount != 0):
	    		for row in cur.fetchall():
	    			returned_hash = row[0]
			if my_hash != returned_hash:
				print "ALERT!! Device hacked!!!!"
                        	publishTopic = "iotSecurity/deviceHacked"
				pub_payload = json.dumps({'device_id':device_id})
                        	mqttc.publish(publishTopic, payload=pub_payload, qos=0, retain=False)
		else:
	    		print "Device not found in DB!!"
        	db.close()

    if msg.topic == "iotSecurity/deviceState":
	print("Hacked device State" +msg.topic+" "+str(msg.qos)+" "+str(msg.payload))    	
        data_arrived = json.loads(msg.payload)
        state = str(data_arrived["setState"])
	device_id = str(data_arrived["device_id"])
	db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user="root",         # your username
                     passwd="whitehats",  # your password
                     db="iotsec")        # name of the data base
        cur = db.cursor()
	device_topic = "iotSecurity/" + device_id
	if state == "disable":
		print "disabling device: " + device_id
		cur.execute("UPDATE acls SET rw = 1 WHERE topic = '" + device_topic + "'")
		db.commit()
	if state == "enable":
		print "enabling device: " + device_id
		cur.execute("UPDATE acls SET rw = 2 WHERE topic = '" + device_topic + "'")
		db.commit()
	db.close()

def on_publish(mqttc, obj, mid):
    print("mid: "+str(mid))

def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))

def on_log(mqttc, obj, level, string):
    print(string)


# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
mqttc = mqtt.Client()
mqttc.username_pw_set(username, password=PASSWORD)
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe

# Uncomment to enable debug messages
#mqttc.on_log = on_log

mqttc.connect("192.168.0.21", 1883, 60)
mqttc.subscribe("iotSecurity/#", 0)

mqttc.loop_forever()

